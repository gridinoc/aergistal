<?php
require 'vendor/Slim/Slim/Slim.php';
require 'vendor/redbean/RedBean/redbean.inc.php';

$app = new Slim(array(
  'debug'       => true,
  'log.enable'  => true,
  'log.path'    => './logs',
  'log.level'   => 4
));
$log = $app->getLog();

R::setup('sqlite:./data/data.sqlite');

//GET route
$app->get('/things/:name', function ($name) {
  
    $thing = R::dispense( 'things' );
    $thing->name = $name;
    $id = R::store($thing);
  
    echo "THING $name = " . $id;
    
    $log->debug($name);
});

$app->run();
